﻿using EnvSafe.Expression;
using Heroius.Extension;
using System;
using System.Text.RegularExpressions;

namespace Heroius.BIU
{
    /// <summary>
    /// BIU算符工厂，为规则中的筛选/执行提供算符
    /// </summary>
    internal class BIUOperatorFactory: SelfFoundOperatorFactory
    {
        /// <summary>
        /// 获取或设置算符执行需要的上下文
        /// </summary>
        public static BIUIterContext Context { get; set; }

        #region Cronus

        /// <summary>
        /// 修改源文件的创建时间
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static string ModifyCreateTime(params string[] parameters)
        {
            Context.fs.CreationTime = Convert.ToDateTime(parameters[0]);
            return "1";
        }
        /// <summary>
        /// 修改源文件的最后写入时间
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static string ModifyEditTime(params string[] parameters)
        {
            Context.fs.LastWriteTime = Convert.ToDateTime(parameters[0]);
            return "1";
        }
        /// <summary>
        /// 修改源文件的最后访问时间
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static string ModifyAccessTime(params string[] parameters)
        {
            Context.fs.LastAccessTime = Convert.ToDateTime(parameters[0]);
            return "1";
        }

        #endregion

        #region Filenamer

        /// <summary>
        /// 正则匹配
        /// </summary>
        /// <param name="parameters">[0]字符串；[1]正则表达式</param>
        /// <returns></returns>
        public static string RegMatch(params string[] parameters)
        {
            Regex re = new Regex(parameters[1]);
            return re.IsMatch(parameters[0]) ? "1" : "0";
        }
        /// <summary>
        /// 匹配源文件名，将替换文件名后的文件复制到目标位置
        /// </summary>
        /// <param name="parameters">[0]正则表达式；[1]替换表达式</param>
        /// <returns></returns>
        public static string RegReplace(params string[] parameters)
        {
            Regex re = new Regex(parameters[0]);
            if (re.IsMatch(Context.fs.Name))
            {
                var tar = $"{Context.fd.DirectoryName}/{re.Replace(Context.fs.Name, parameters[1])}";
                if (!Context.fd.Directory.Exists)
                {
                    Context.fd.Directory.Create();
                }
                Context.fs.CopyTo(tar, true);
                return "1";
            }
            else
            {
                return "0";
            }
        }

        #endregion

        /// <summary>
        /// 将多个字符串合并
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static string StrMerge(params string[] parameters)
        {
            return parameters.Merge();
        }


        //public static string RBFO_CopyTo(params string[] parameters)
        //{

        //}

        //public static string RBFO_ExistsIn(params string[] parameters)
        //{

        //}
    }
}
