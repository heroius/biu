﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroius.BIU
{
    /// <summary>
    /// 用于存储迭代过程中的需要使用的表达式变量
    /// </summary>
    public class BIUIterContext
    {
        /// <summary>
        /// 当前迭代的相对路径
        /// </summary>
        public string currentRela { get; set; }
        /// <summary>
        /// 当前迭代位置的源目录
        /// </summary>
        public DirectoryInfo src { get; set; }
        /// <summary>
        /// 当前迭代位置的目标目录
        /// </summary>
        public DirectoryInfo dst { get; set; }
        /// <summary>
        /// 本次run的记录实例
        /// </summary>
        public BIURecord rec { get; set; }
        /// <summary>
        /// 源文件info
        /// </summary>
        public FileInfo fs { get; set; }
        /// <summary>
        /// 目标文件info
        /// </summary>
        public FileInfo fd { get; set; }

        /// <summary>
        /// 计数已满足条件的执行（在执行前计数）
        /// </summary>
        public int iter { get; set; } = 0;
    }
}
