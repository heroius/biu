﻿using EnvSafe.Expression;
using Heroius.Extension;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Heroius.BIU
{
    /// <summary>
    /// 代表一次备份操作的计划
    /// </summary>
    public class BIUPlan: ObservableEntity
    {
        /// <summary>
        /// 代表一次备份操作的计划
        /// </summary>
        public BIUPlan()
        {
            Rules = new ObservableCollection<Rule>();
            Variables = new ObservableCollection<ValuePair<string, string>>();
        }

        /// <summary>
        /// 备份源文件夹
        /// </summary>
        public string From { get { return _From; } set { _From = value; RaisePropertyChangedEvent("From"); } } string _From;
        /// <summary>
        /// 备份目标文件夹
        /// </summary>
        public string To { get { return _To; } set { _To = value; RaisePropertyChangedEvent("To"); } } string _To;
        /// <summary>
        /// 备份规则
        /// </summary>
        public ObservableCollection<Rule> Rules { get; set; }
        /// <summary>
        /// 自定义变量
        /// </summary>
        public ObservableCollection<ValuePair<string, string>> Variables { get; set; }
    }

    /// <summary>
    /// 代表一条执行规则
    /// </summary>
    public class Rule
    {
        /// <summary>
        /// 约束执行该规则需要满足的文件状态
        /// </summary>
        [Description("文件状态")]
        public FileState State { get; set; }
        /// <summary>
        /// 约束执行该规则需要满足的过滤表达式
        /// </summary>
        [Description("过滤表达式")]
        public string FilterExp { get; set; }
        /// <summary>
        /// 当所有约束都满足时，要执行的动作
        /// </summary>
        [Description("执行动作")]
        public ExecutionOption Execution { get; set; }

        /// <summary>
        /// 过滤器，由引擎在执行初期根据<see cref="FilterExp"/>生成
        /// </summary>
        internal Procedure Filter { get; set; }

        /// <summary>
        /// 仅当<see cref="Execution"/>为<see cref="ExecutionOption.Custom"/>时使用，执行表达式
        /// </summary>
        [Description("执行表达式")]
        public string CustomExp { get; set; }

        /// <summary>
        /// 执行器，由引擎在执行初期根据<see cref="Execution"/>和<see cref="CustomExp"/>生成
        /// </summary>
        internal Procedure Custom { get; set; }
    }

    /// <summary>
    /// 文件状态
    /// </summary>
    public enum FileState
    {
        /// <summary>
        /// 某个文件（名）仅在源文件目录下发现，而目标未发现
        /// </summary>
        [Description("只存在于源")]
        OnlySrc,
        /// <summary>
        /// 某个文件（名）在源和目标均发现
        /// </summary>
        [Description("均已存在")]
        Both
    }

    /// <summary>
    /// 列举执行动作
    /// </summary>
    public enum ExecutionOption
    {
        /// <summary>
        /// 跳过该文件
        /// </summary>
        [Description("跳过")]
        Skip,
        /// <summary>
        /// 删除另一端未发现的文件
        /// </summary>
        [Description("从目标删除")]
        Delete,
        /// <summary>
        /// 从源到目标复制（或覆盖）
        /// </summary>
        [Description("执行复制和覆盖")]
        Copy,
        /// <summary>
        /// 自定义执行表达式
        /// </summary>
        [Description("自定义")]
        Custom
    }
}
