﻿using System;

namespace Heroius.BIU
{
    /// <summary>
    /// run 记录
    /// </summary>
    public class BIURecord
    {
        public DateTime BackupTime { get; set; } = DateTime.Now;
        public string RunMessage { get; set; } = "";
        public string UserRemark { get; set; } = "";
        public string From { get; set; } = "";
        public BIURunResult Result { get; set; } = BIURunResult.Failed;
    }

    public enum BIURunResult
    {
        Succeed,
        Interupt,
        Failed
    }
}
